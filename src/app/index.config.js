(function () {
    'use strict';

    angular
        .module('fcollectionWeb')
        .config(config);

    /** @ngInject */
    function config($logProvider, $compileProvider, debugEnabled) {
        $logProvider.debugEnabled(debugEnabled);
        $compileProvider.debugInfoEnabled(debugEnabled);
    }

})();
