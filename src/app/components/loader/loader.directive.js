(function() {
  'use strict';

  angular
    .module('fcollectionWeb')
    .directive('loader', loader);

  /** @ngInject */
  function loader() {
    var directive = {
      restrict: 'E',
      template: '<div class="loader"><div></div></div>',
      link: loaderLink
    };

    return directive;

    function loaderLink(scope, element, attr) {



    }
  }

})();
