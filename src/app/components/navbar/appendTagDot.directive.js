(function () {
    'use strict';

    angular
        .module('fcollectionWeb')
        .directive('appendTagDot', appendTagDot);

    /** @ngInject */
    function appendTagDot($rootScope, $window, $state) {
        return function(scope, elem, attrs) {
            if (!scope.$last) {
                var dot = angular.element('<div class="sub-nav-tag">\
                                <i class="icon-dot"></i>\
                            </div>');
                elem.after(dot);
            }
        };
    }

})();
