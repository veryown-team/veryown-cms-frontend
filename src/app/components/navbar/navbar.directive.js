(function () {
    'use strict';

    angular
        .module('fcollectionWeb')
        .directive('mainNav', mainNav);

    /** @ngInject */
    function mainNav($rootScope, $window, $state) {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/navbar/navbar.html',
            controller: NavbarController,
            controllerAs: 'navbar',
            bindToController: true,
            scope: true,
            link: mainNavLink

        };

        return directive;

        /** @ngInject */
        function NavbarController($rootScope, $window, $scope, sections, tags, social) {

            var vm = this;
            $scope.openSubnav = false;
            $scope.social = social;

            $scope.closeSubnav = function () {
                $scope.openSubnav = false;
            }

            sections.then(function (sections) {
                vm.sections = sections;
            });
            tags.then(function (tags) {
                tags.splice(6);
                vm.tags = tags;
            });
        }


        function mainNavLink(scope, elem, attrs) {
            //   var shouldExpandHeader = true;
            //   if ($state.name == "aritcle") {
            //     shouldExpandHeader = false;
            //   }

            resetScroll(elem);
            //initScrollHide(elem);


            $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParam) {
                //   if (toState.name == 'article') {
                //       shouldExpandHeader = false;
                //   } else {
                //       shouldExpandHeader = true;
                //   }
                scope.openSubnav = false;
                resetScroll(elem);
            })

            function resetScroll(elem) {
                // var content = $(window);
                // var headerWrapper = $(elem).find('.header-wrapper');
                // headerWrapper.removeClass('scrolled-down');
                // // if (shouldExpandHeader) {
                // //
                // // } else {
                // //   headerWrapper.addClass('scrolled-down');
                // // }
                $window.scrollTo(0, 0)
            }

            //function initScrollHide(elem) {
            //  var content = $(window);
            //  var scrollStartPoint = 55;
            //  var scrollEndPoint = Infinity;
            //
            //  if (content.width() > 768) {
            //    scrollStartPoint = 255;
            //    scrollEndPoint = 500;
            //  }
            //  var deltaOffset = 50;
            //  var lastScroll = 0;
            //
            //  var currentDirection = 0;
            //  var startDirection = 0;
            //
            //  function scrollDirection(start, end) {
            //    return (start - end) < 0 ? 1 : -1;
            //    // Scrolling Down -> == 1
            //    // Scrolling Up -> == -1
            //  }
            //
            //  content.scroll(function (event) {
            //  //   if (!shouldExpandHeader) {
            //  //     return;
            //  //   }
            //    var scroll = content.scrollTop();
            //    var headerWrapper = $(elem)
            //      .find('.header-wrapper');
            //
            //    if (currentDirection == scrollDirection(lastScroll, scroll)) { // Still moving same direction
            //      var currentDelta = Math.abs(scroll - startDirection);
            //      if (currentDelta > deltaOffset) { // Delta is enough to change classes
            //        if (scroll > scrollStartPoint && scrollDirection(lastScroll, scroll) > 0) { // changing classes
            //          headerWrapper.addClass('scrolled-down');
            //
            //        } else {
            //          if (scroll < scrollEndPoint) {
            //            headerWrapper.removeClass('scrolled-down');
            //          }
            //        }
            //      } else { // delta is not enough for change
            //
            //      }
            //    } else { // direction has changed
            //      currentDirection = scrollDirection(lastScroll, scroll);
            //      startDirection = scroll;
            //    }
            //
            //    lastScroll = scroll;
            //  });
            //}
        }
    }

})();
