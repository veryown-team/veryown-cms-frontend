(function () {
    'use strict';

    var coverCellComponent = {
        bindings : {
            article: '<'
        },
        templateUrl: 'app/states/grid/components/coverCellComponent/coverCellComponent.tpl.html'
    }

    angular
        .module('fcollectionWeb')
        .component('coverCell', coverCellComponent);
})();