(function () {
    'use strict';

    angular
        .module('fcollectionWeb')
        .controller('GridController', GridController);

    /** @ngInject */
    function GridController(fcAPI, $stateParams, $scope) {
        $scope.isLoading = true;
        var isEmptyFlag = true;

        var vm = this;
        vm.page = {
            currentPage: 1,
            loadMore: loadMore,
            isEmpty: isEmpty,
            totalLoaded: 0,
            totalInSection: 1,
            isLastPage: isLastPage,
            isLoadingMore: false
        }

        vm.getImageBackground = function (img) {
            if (img) {
                return {'background-image': 'url(' + img + ')'};
            } else {
                return {};
            }
        };

        // 1 + 6
        vm.blocks = [];

        var sectionId = $stateParams.sectionId;
        var tagId = $stateParams.tagId;
        loadArticles();

        ///////


        function loadArticles() {
            var currentPage = vm.page.currentPage;
            if (vm.page.totalLoaded !== 0) {
                vm.page.isLoadingMore = true;
            }
            if (sectionId) {
                fcAPI.getArticlesWithSection(sectionId, currentPage).then(loadArticlesSuccess);
                vm.page.currentPage++;
            } else if (tagId) {
                fcAPI.getArticlesWithTag(tagId, currentPage).then(loadArticlesSuccess);
                vm.page.currentPage++;
            }

            function loadArticlesSuccess(results) {
                vm.blocks.push(results);
                if (isEmptyFlag && (results.articles.length > 0 || results.cover.length > 0)) {
                    isEmptyFlag = false;
                }
                $scope.isLoading = false;
                vm.page.totalInSection = +results.total;
                vm.page.totalLoaded += results.articles.length;
                vm.page.totalLoaded += results.cover.length;
                vm.page.isLoadingMore = false;
            }
        }

        function loadMore() {
            loadArticles();
        }

        function isEmpty() {
            return $scope.isLoading || isEmptyFlag
        }

        function isLastPage() {
            if (isEmptyFlag) {
                return true;
            }
            return vm.page.totalLoaded >= vm.page.totalInSection;
        }
    }

})();
