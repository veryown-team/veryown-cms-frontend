(function() {
    'use strict';

    angular
        .module('fcollectionWeb')
        .controller('PageController', PageController);

    /** @ngInject */
    function PageController(fcAPI, $stateParams, $scope) {
        $scope.isLoading = true;
        var vm = this;
        var pageId = $stateParams.pageId
        vm.page = {};

        fcAPI.getPage(pageId).then(function(page) {
            vm.page = page;
            $scope.isLoading = false;
        })
    }

})();
