(function() {
    'use strict';

    angular
        .module('fcollectionWeb')
        .controller('ArticleController', ArticleController);

    /** @ngInject */
    function ArticleController(fcAPI, $stateParams, $scope) {
        $scope.isLoading = true;
        var vm = this;
        var articleId = $stateParams.articleId
        vm.article = $stateParams.article;

        fcAPI.getArticle(articleId).then(function(article) {
            vm.article = article;
            $scope.isLoading = false;
        })
    }

})();
