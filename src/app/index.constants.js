(function() {
  'use strict';

  angular
    .module('fcollectionWeb')
    .constant('social', {
        "inst" : 'https://www.instagram.com/fcollectionby/',
        "fb" : 'https://www.facebook.com/FashionCollectionBY/',
        "vk" : 'https://vk.com/fashioncollectionbelarus'
    });
})();
