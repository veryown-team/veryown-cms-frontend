(function () {
    'use strict';

    angular
        .module('fcollectionWeb', [
            'ngSanitize',
            'ui.router',
            'angularMoment',
            'angular-click-outside'
        ]);

})();
