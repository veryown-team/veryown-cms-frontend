(function() {
    'use strict';

    angular
        .module('fcollectionWeb')
        .controller('IndexController', IndexController);

    /** @ngInject */
    function IndexController($scope, $window) {
        $scope.openBlank = function (url) {
            $window.open(url, '_blank');
        };
    }
})();
