(function () {
  'use strict';
  angular
    .module('fcollectionWeb')
    .factory('fcAPI', fcAPI);

  /* @ngInject */
  function fcAPI($http, $log, stringTransformer, api) {
    var serverUrl = api;
    var fcAPI = {
      getArticle: getArticle,
      getArticlesWithTag: getArticlesWithTag,
      getArticlesWithSection: getArticlesWithSection,

      getTags: getTags,
      getSections: getSections

  };
    return fcAPI;

    //////////


    function getArticle(id) {
      return $http.get(serverUrl + '/articles/' +id)
        .then(function (response) {
          var article = response.data;
          $log.info("article:", article);
          // convert fulltext
          var fullText = article.fullText;
          article.fullText = stringTransformer.b64_to_utf8(fullText);
          return article;
      });
    }

    function getArticlesWithSection(sectionId, page) {
      return $http.get(serverUrl + '/sections/' + sectionId, {
          params: {
              page: page
          }
      })
        .then(function(results){
            var articles = results.data;
            var total = results.headers('total');
            $log.info(sectionId, articles);
            articles.total = total;
            return articles;
        });
    }

    function getArticlesWithTag(tagId, page) {
        return $http.get(serverUrl + '/tags/' + tagId, {
            params: {
                page: page
            }
        })
          .then(function(results){
              var articles = results.data;
              var total = results.headers('total');
              $log.info(tagId, articles);
              articles.total = total;
              return articles;
          });
    }


    function getSections() {
      return $http.get(serverUrl + '/sections')
        .then(function (response) {
          var sections = response.data;
          $log.info("sections:", sections);
          return sections;
      });
    }

    function getTags() {
      return $http.get(serverUrl + '/tags')
        .then(function (response) {
          var tags = response.data;
          $log.info("tags:", tags);
          return tags;
      });
    }
  }
})();
