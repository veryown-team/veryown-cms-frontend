(function(){
    'use strict';
    angular
        .module('fcollectionWeb')
        .factory('sections', sections);

    /* @ngInject */
    function sections(fcAPI){        
        return fcAPI.getSections();
    }
})();
