(function () {
  'use strict';

  angular
    .module('fcollectionWeb')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    var gridController = 'GridController';
    var gridControllerAs = 'gridController';

    $stateProvider
      .state('article', {
        url: '/a/:articleId',
        params: {
            article: {}
        },
        templateUrl: 'app/states/article/article.html',
        controller: 'ArticleController',
        controllerAs: 'articleController'
      })
      .state('section', {
        url: '/:sectionId',
        params: {
          sectionId: 'magazine',
        },
        templateUrl: 'app/states/grid/grid.html',
        controller: 'GridController',
        controllerAs: 'gridController',
        resolve: {
            isSectionExist: function (sections, $stateParams, $q) {
                var defer = $q.defer();
                var sectionId = $stateParams.sectionId;
                //
                if (!sectionId) {
                    defer.reject("sectionId is not passed");
                }

                if (sectionId == "magazine") {
                    defer.resolve("It it main page");
                } else {
                    //it is not magazine, try check another section;
                    defer.resolve(true)
                }
                return defer.promise;
            }
        }
      })
      .state('tag', {
        url: '/t/:tagId',
        templateUrl: 'app/states/grid/grid.html',
        controller: 'GridController',
        controllerAs: 'gridController',
      })
      .state('page', {
          url: '/p/:pageId',
          templateUrl: 'app/states/page/page.html',
          controller: 'PageController',
          controllerAs: 'pageController'
      });

    $urlRouterProvider.otherwise('/');
  }

})();
